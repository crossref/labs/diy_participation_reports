# DIY Participation Reports

Show people how to use Jupyter Notebooks and the Crossref REST API to create a DIY version of Crossref's participation reports.

Click on the button below to launch the notebook in dashboard mode.


[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fdiy_participation_reports/master?filepath=index.ipynb)
